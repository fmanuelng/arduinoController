#include <SPI.h>
#include <PN532.h>
#include <PN532_SPI.h>
#include <ArduinoJson.h>
#include <NfcAdapter.h>
#include <WString.h>

using namespace std;

#define SCK         13
#define MOSI        11
#define SS          10
#define MISO        12

void handleSerial();
String rawString;
char opc;
PN532_SPI pn532_spi(SPI, SS);           //Instancia de conexion al lector NFC
NfcAdapter NFC = NfcAdapter(pn532_spi); //instancia del adaptador NFC
StaticJsonBuffer<200> jsonBuf;          //Instancia de buffer para el almacenamiento de datos JSON
void setup() {
  Serial.begin(9600);
  if (!Serial) {
    delay(100);
  }
  NFC.begin();
}

void loop() {
  handleSerial();
  JsonObject& rootOpc = jsonBuf.parseObject(rawString);
  opc = rootOpc["cmd"];
  switch (opc){
    case 'r':
      ReadTag();
      break;
      case 'w':
        WriteTag(rootOpc["amount"]);
        break;
}

void ReadTag() {
  Serial.println("\nFavor de acercar su tarjeta u telefono movil al dispositivo");
  if (NFC.tagPresent()) {
    NfcTag tag = NFC.read();
    Serial.println("UID: ");
    Serial.println(tag.getUidString());
    if (tag.hasNdefMessage()) {
      NdefMessage message = tag.getNdefMessage();
      Serial.print("\nValor localizado en el tag");
      Serial.print(message.getRecordCount());
      Serial.print(" NFC Tag Record");
      if (message.getRecordCount() != 1) {
        Serial.print("s");
      }
      Serial.println(".");

      int recordCount = message.getRecordCount();
      for (int i = 0; i < recordCount; i++) {
        Serial.print("\nNDEF Record "); Serial.println(i + 1);
        NdefRecord record = message.getRecord(i);

        int payloadLength = record.getPayloadLength();
        byte payload[payloadLength];
        record.getPayload(payload);

        String payloadAsString = ""; // Processes the message as a string vs as a HEX value
        for (int c = 0; c < payloadLength; c++) {
          char val = (char)payload[c];
          payloadAsString += String(val);
        }
        Serial.print("  Information (as String): ");
        //String payloadAsString = String(payloadAsCharArray);
        const char* result = payloadAsString.c_str();
       JsonObject& root = jsonBuf.parseObject(result);
        root.printTo(Serial);
      }
    }
  }
  delay(10000);
}

void WriteTag(float amount) {
  JsonObject& root = jsonBuf.createObject();
  Serial.println("\nFavor de acercar su tarjeta u telefono movil al dispositivo");
  if(NFC.tagPresent()){
    NfcTag tag = NFC.read();
    root["id"] = tag.getUidString();
    root["amount"] = amount;
    String dta;
    root.printTo(dta);
    NdefMessage message = NdefMessage();
    message.addTextRecord(dta);
    boolean success = NFC.write(message);
    if(success){
      Serial.println("Informacion de la tarjeta guardada correctamente");
      } else {
        Serial.println("Error de escritura en la tarjeta !");
        }
        delay(10000);
    }
}

void handleSerial() {
  while (Serial.available()) {
   delay(1);
   if (Serial.available() > 0){
     char c = Serial.read();
     rawString += c;
   }
  }
  }
}
