# ArduinoController

Controlador para usar el dispositivo PN532 (Lertor/Escritor NFC) en el arduino

<!-- TOC -->

- [ArduinoController](#arduinocontroller)
    - [Funcionalidades](#funcionalidades)
    - [Instalacion](#instalacion)
    - [Uso](#uso)
    - [Herramientas Utilizadas](#herramientas-utilizadas)
        - [NFC Adapter](#nfc-adapter)
        - [Seeed-Studio PN532 library](#seeed-studio-pn532-library)
        - [ArduinoJSON](#arduinojson)

<!-- /TOC -->

## Funcionalidades
1. Capacidad de leer la data contenida en una tarjeta NFC Chelitos y retornar su informacion en un JSON
2. Escribir un JSON recibido desde el serial con la estructura de Chelitos y escribirlo en una tarjeta NFC  


## Instalacion
Antes que todo debemos cumplir con los siguientes requisitos para poder utilizar este controlador, los cuales son:
1. Poseer un arduino (Ya sea el Arduino Uno o el Arduino Mega)
2. Tener instalado el IDE Arduino en nuestro computador. Puede descargarlo [aqui](https://www.arduino.cc/en/Main/Software)
3. Descargar las siguientes librerias a ser usadas por el controlador, estas deben de estar en la carpeta libraries que el IDE crea en su carpeta de usuario, entre las cuales estan:
    1.  [NfcAdapter](#nfc-adapter)
    2.  [PN532 Library](#seeed-studio-pn532-library)
    3.  [Arduino Json Library](#arduinojson)  

## Uso
Solo debe de abrir este codigo en el IDE de arduino y luego de esto fijarse en el puerto en el cual el arduino resulte estar conectado, esto debe de tenerlo en cuenta a la hora de ejecutar [nfcUpdater](https://gitlab.com/fmanuelng/nfcUpdater) ya que el mismo depeende del puerto en el cual el mismo este conectado para efectuar la comunicacion con el mismo. Luego de esto debe de subir este codigo a su arduino precionando ![UploadImg](http://karma-laboratory.com/workshops/arduinoforprogrammers/images/verify_and_upload.png)
## Herramientas Utilizadas

### NFC Adapter
**[NfcAdapter](https://github.com/don/NDEF)**: Adaptador (wrapper) para la libreria controladora de la PN532.

### Seeed-Studio PN532 library 

**[Seeed-Studio PN532 library](https://github.com/Seeed-Studio/PN532)**: Controlador para el lector NFC PN532

### ArduinoJSON

**[Arduino Json Library](https://arduinojson.org/doc/installation/)**: Libreria para la serializacion y deserializacion de datos de tipo JSON